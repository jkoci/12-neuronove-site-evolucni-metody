#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module deals with parsing patterns file into sane dictionary
"""
import os
import time
import types
import logging
import logging.config

import numpy as np
import tkinter as tk
from PIL import Image

logging.config.fileConfig('logging.conf')
LOGGER = logging.getLogger('mainLogger')


EXPECTED_CHARACTERS = set("01\n")

E_UNEXP_CHARS = "Line {}, found unexpected characters {}"
E_ROWS = "Pattern '{}' has wrong number of rows: {}, expected {}"
E_COLS = """Pattern '{}', line {} has wrong number of cols: {}, \
expected {}"""


def parse_pattern_file(fname):
    """ Turns file of patterns into dict of patterns """
    patterns = dict()
    with open(fname) as fr:
        for lineno, line in enumerate(fr):
            if line[0] == ";":
                # it's either pattern name
                pname = line[1:].strip()
                LOGGER.debug("Line %d, found pattern name '%s'",
                             lineno, pname)
                patterns[pname] = list()

            else:
                # or else data line
                chrs = set(line)
                rest = chrs - EXPECTED_CHARACTERS
                if rest:
                    msg = E_UNEXP_CHARS.format(lineno, list(rest))
                    LOGGER.critical(msg)
                    raise ValueError(msg)
                else:
                    patterns[pname].append(line.strip())
    return patterns


def check_patterns(patterns):
    """
    Checks if all patterns are of expected dimensions,
    that is having rows as first pattern and columns as
    first line of first pattern
    """
    first_item = _first(patterns)
    rows = len(patterns[first_item])
    cols = len(patterns[first_item][0])
    LOGGER.debug("Expected size of patterns: %dr×%dc", rows, cols)

    for key, val in patterns.items():
        curr_rows = len(val)
        if curr_rows != rows:
            msg = E_ROWS.format(key, curr_rows, rows)
            LOGGER.critical(msg)
            raise ValueError(msg)
        for ln, row in enumerate(val):
            curr_cols = len(row)
            if curr_cols != cols:
                msg = E_COLS.format(key, ln, curr_cols, cols)
                LOGGER.critical(msg)
                raise ValueError(msg)

        LOGGER.debug("Pattern '%s' is in good shape", key)

    return rows, cols


def transform_patterns(patterns):
    """ Transforms list-of-lists structure into flat structure """
    new_pats = dict()

    for key, val in patterns.items():
        pat = [int(item) for sublist in val for item in sublist]
        new_pats[key] = binary_to_bipolar(pat)

    return new_pats


def mk_patterns(filename="infile.txt"):
    """ Prepare patterns for Hopfield() """
    patterns = parse_pattern_file(filename)
    rows, cols = check_patterns(patterns)
    patterns = transform_patterns(patterns)
    dimensions = tuple([rows, cols])

    return dimensions, patterns


def pattern_to_image(pattern, shape):
    """ Converts pattern+shape into PIL Image """
    img = shape_img(pattern, shape)
    im = Image.fromarray(img, 'L')
    im = im.resize((200, 200), resample=Image.NEAREST)
    return im


def shape_img(data, shape):
    """ Reshapes pattern data to needed shape """
    img = np.array(bipolar_to_binary(data), dtype=np.uint8)\
            .reshape(shape[0], shape[1])
    img = np.uint8(img * 255)
    return img


def _safe_dir(path):
    try:
        os.makedirs(path)
    except FileExistsError:
        pass


def _first(iterator):
    return next(iter(iterator))


class HopfieldNetwork():
    """ This is class implementing Hopfield's network """
    def __init__(self, par_frame=None, limit=0.5, max_iter=1000):
        self.weight = None
        self.patterns = None
        self.shape = None
        self.limit = limit
        self.max_iter = max_iter
        self.full = True

        self.tk = types.SimpleNamespace()
        self.tk.parent = par_frame or tk.Tk()
        self.tk.canvas = None
        self.tk.grid = dict()

    def _init_parent(self, cellsize):
        can = self.tk.canvas = tk.Canvas(self.tk.parent,
                                         width=cellsize*self.shape[1],
                                         height=cellsize*self.shape[0])
        self.tk.titlevar = tk.StringVar()
        self.tk.title = tk.Label(self.tk.parent,
                                 text="N/a",
                                 textvariable=self.tk.titlevar)
        # pack items
        self.tk.title.grid(column=0, row=1, sticky='NSWE')
        can.grid(column=0, row=2, sticky='NSWE')

        cnt = 0
        for row in range(self.shape[0]):
            for col in range(self.shape[1]):
                x = col * cellsize
                y = row * cellsize
                self.tk.grid[cnt] = can.create_rectangle(x, y,
                                                         x+cellsize,
                                                         y+cellsize,
                                                         fill='white')
                cnt += 1

    def learn(self, patterns, shape):
        """ Calculates weight matrices for given patterns """
        self.forget()
        self.patterns = patterns
        self.shape = shape
        for name, pat in patterns.items():
            LOGGER.debug("Learning pattern '%s'", name)
            length = len(pat)
            weight = np.zeros([length]*2)
            for i in range(length):
                for j in range(i, length):
                    if i == j:
                        weight[i, j] = 0
                    else:
                        weight[i, j] = pat[i] * pat[j]
                        weight[j, i] = weight[i, j]
            if self.weight is None:
                self.weight = weight
            else:
                self.weight += weight

            self._init_parent(20)

    def fix(self, broken):
        """ Attempts to fix broken pattern """
        length = len(broken)
        work = broken
        LOGGER.debug("Fixing")
        for i in range(1, self.max_iter):
            LOGGER.debug("Runnign iteration %d", i)
            has_changed = False

            for n_id in range(length):
                prev = work[n_id]
                sum_ = 0
                for row in range(length):
                    sum_ += work[row] * self.weight[row, n_id]

                work[n_id] = sign(sum_)
                post = work[n_id]
                LOGGER.debug("Working on neuron %d" +
                             " (prev %d, curr %d)",
                             n_id+1, prev, post)
                if self.full:
                    self.show_plt_img(work)
                    self.set_title("Iteration {}, neuron {}/{}"
                                   .format(i, n_id+1, length))
                    time.sleep(0.05)
                if prev != post:
                    has_changed = True

            if not has_changed:
                LOGGER.debug("Stable iteration at iter %d", i)
                self.set_title("Finished at iteration {}".format(i))
                self.show_plt_img(work)
                break  # we've hit stable iteration
            else:
                LOGGER.debug("Generation %d, neurons changed" +
                             ", looping", i)

    def forget(self):
        """ Clears neural network """
        self.weight = None
        self.patterns = None
        self.shape = None

    def set_full(self, integer):
        self.full = True if integer > 0 else False

    def show_plt_img(self, data):
        """ Shows image """
        # img = shape_img(data, shape)
        # plt.imshow(img, cmap="binary")
        # cur_axes = plt.gca()
        # cur_axes.axes.get_xaxis().set_visible(False)
        # cur_axes.axes.get_yaxis().set_visible(False)
        # plt.show(block=False)
        # plt.pause(0.001)
        can = self.tk.canvas
        for i in range(len(data)):
            grid_id = self.tk.grid[i]
            col = 'white' if data[i] == 1 else 'black'
            can.itemconfig(grid_id, fill=col)
            # tk update window
            self.tk.parent.update()

    def set_title(self, string):
        self.tk.titlevar.set(string)


def noise(pattern, perc):
    """
    Applies selected noise to given pattern
    param:pattern memory pattern to be noised
    param:perc    ratio of 1s in noise vector (given in decimal,
                  e.g. set 25% as 0.25)
    Returns: noised pattern
    """
    pattern = bipolar_to_binary(pattern)
    threshold = 1 - perc
    noise_v = np.random.uniform(0, 1, len(pattern))
    noise_v = [0 if x < threshold else 1 for x in noise_v]
    result = [sum(tpl) % 2 for tpl in zip(pattern, noise_v)]
    return binary_to_bipolar(result)


def binary_to_bipolar(vector):
    """ Transforms 0/1 vector into -1/1 vector """
    return [(-1 if x == 0 else 1) for x in vector]


def bipolar_to_binary(vector):
    """ Transforms -1/1 vector into 0/1 vector """
    return [(0 if x == -1 else 1) for x in vector]


def sign(number):
    if number < 0:
        ret = -1
    elif number > 0:
        ret = 1
    else:
        ret = 0
    return ret


def create_thumbs(pats, dims):
    dirs = os.path.join(os.getcwd(), "imgs")
    _safe_dir(dirs)
    for name, pat in pats.items():
        im = pattern_to_image(pat, dims)
        LOGGER.debug("Saving pattern '%s'", name)
        im.save(os.path.join(dirs, "{}.png".format(name)))


def _main():
    dims, pats = mk_patterns()
    create_thumbs(pats, dims)

    neunet = HopfieldNetwork(limit=0.7)
    neunet.learn(pats, dims)
    broken = noise(pats[_first(pats)], perc=0.3)
    neunet.show_plt_img(broken)
    time.sleep(5)
    neunet.fix(broken)
    neunet.tk.parent.mainloop()


if __name__ == "__main__":
    _main()
