#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
GUI app for Hopfield neu net
"""

import os
import sys
import types
import tkinter as tk
from PIL import Image, ImageTk

from hopfield import HopfieldNetwork, mk_patterns, noise
from hopfield import create_thumbs


class Application():
    """ The GUI APP """
    def __init__(self):
        self.tkvars = types.SimpleNamespace()
        self.pat = types.SimpleNamespace()
        self.frames = types.SimpleNamespace()

        self.frames.mainwindow = tk.Tk()
        self.frames.mainwindow.title("Hopfield GUI")

        self.image = None
        self.canvas = None
        self.noised = None

        self.frames.leftframe = tk.Frame(self.frames.mainwindow)
        self.frames.leftframe.grid(column=0, row=1, sticky='NSEW')
        self.frames.rightframe = tk.Frame(self.frames.mainwindow)
        self.frames.rightframe.grid(column=1, row=1, sticky='NSEW')

        self.frames.topframe = tk.Frame(self.frames.leftframe)
        self.frames.topframe.grid(column=0, row=1, sticky='NSEW')

        self.frames.midframe = tk.Frame(self.frames.leftframe)
        self.frames.midframe.grid(column=0, row=2, sticky='NSEW')

        self.frames.botframe = tk.Frame(self.frames.leftframe)
        self.frames.botframe.grid(column=0, row=3, sticky='NSEW')

        self._init_vars()
        self._init_window()

        self.network = HopfieldNetwork(par_frame=self.frames.rightframe)

    def _init_vars(self):
        self.tkvars.pat_file = tk.StringVar()
        self.tkvars.pat_file.set("infile.txt")
        self.tkvars.slider = tk.DoubleVar()
        self.tkvars.slider.set(0.25)
        self.tkvars.chkbx = tk.IntVar()
        self.tkvars.chkbx.set(1)

        self.pat.pattern_no = None
        self.pat.dims = None
        self.pat.pats = None
        self.pat.patterns = None

    def _init_window(self):
        self._init_top_frame()
        self._init_mid_frame()
        self._init_bot_frame()

    def _init_top_frame(self):
        lbl_file = tk.Label(self.frames.topframe, anchor='e',
                            text="Patterns file: ")
        lbl_file.grid(column=0, row=1, sticky='W')

        entry_file = tk.Entry(self.frames.topframe,
                              textvariable=self.tkvars.pat_file)
        entry_file.grid(column=1, row=1, sticky='E')

        btn_load = tk.Button(self.frames.topframe,
                             command=self._on_btn_load,
                             text="Load file")
        btn_load.grid(column=0, row=2, columnspan=2)

    def _init_mid_frame(self):
        self.canvas = tk.Canvas(self.frames.midframe,
                                height=200,
                                width=200)
        self.canvas.grid(column=0, row=1, columnspan=2)

        btn_prev = tk.Button(self.frames.midframe,
                             command=self._on_btn_prev,
                             text="< Prev")
        btn_prev.grid(column=0, row=2, sticky='EW')

        btn_next = tk.Button(self.frames.midframe,
                             command=self._on_btn_next,
                             text="Next >")
        btn_next.grid(column=1, row=2, sticky='EW')

        slider = tk.Scale(self.frames.midframe,
                          label="Noise ratio",
                          showvalue=1,
                          from_=0.0, to=1.0,
                          resolution=0.01,
                          orient='horizontal',
                          variable=self.tkvars.slider,
                          length=250)
        slider.grid(column=0, row=3, columnspan=2)

        checkbox = tk.Checkbutton(self.frames.midframe,
                                  text="Show full progress",
                                  variable=self.tkvars.chkbx)
        checkbox.grid(column=0, row=4, sticky='W')

    def _init_bot_frame(self):
        btn_noise = tk.Button(self.frames.botframe,
                              command=self._on_btn_noise,
                              text="Noise the image")
        btn_noise.grid(column=0, row=0, sticky='NSEW')
        btn_go = tk.Button(self.frames.botframe,
                           command=self._on_btn_go,
                           text="Go!")
        btn_go.grid(column=1, row=0, sticky='NSEW')

        btn_quit = tk.Button(self.frames.botframe,
                             command=self._on_btn_quit,
                             text="Quit")
        btn_quit.grid(column=2, row=0, sticky='NSEW')

    def _draw_image_on_canvas(self, imagepath):
        img = Image.open(os.path.join(os.getcwd(),
                                      "imgs",
                                      "{}.png".format(imagepath)))
        image = ImageTk.PhotoImage(img)
        self.image = image
        self.canvas.create_image(100, 100, image=image)

    def _on_btn_quit(self):
        print("Bye...")
        self.frames.mainwindow.destroy()
        sys.exit(0)

    def _on_btn_load(self):
        print("Loading file '{}'".format(self.tkvars.pat_file.get()))
        dims, pats = mk_patterns(self.tkvars.pat_file.get())
        create_thumbs(pats, dims)
        self.network.forget()
        self.image = None
        self.pat.pattern_no = None
        self.pat.dims = None
        self.pat.pats = None
        self.pat.patterns = None
        self.noised = None

        self.network.learn(pats, dims)

        self.pat.dims = dims
        self.pat.pats = list(pats.keys())
        self.pat.patterns = pats
        self.pat.pattern_no = 0
        print("Network learned")

        self._update_sel_pat()

    def _on_btn_go(self):
        self.network.set_full(self.tkvars.chkbx.get())
        self.network.fix(self.noised)

    def _on_btn_prev(self):
        self.pat.pattern_no -= 1
        self.pat.pattern_no %= len(self.pat.pats)
        self._update_sel_pat()

    def _on_btn_next(self):
        self.pat.pattern_no += 1
        self.pat.pattern_no %= len(self.pat.pats)
        self._update_sel_pat()

    def _on_btn_noise(self):
        pat_name = self.pat.pats[self.pat.pattern_no]
        pat = self.pat.patterns[pat_name]
        self.noised = noise(pat, self.tkvars.slider.get())
        self.network.show_plt_img(self.noised)

    def _update_sel_pat(self):
        what = self.pat.pats[self.pat.pattern_no]
        self._draw_image_on_canvas(what)

    def run(self):
        """ Runs the main window """
        self.frames.mainwindow.mainloop()

    def spam(self):
        """ Dummy method to stop pylint complaining. I know, I should
        disable the warning, but this is easier """


def _main():
    app = Application()
    app.run()


if __name__ == '__main__':
    _main()
