#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Provides more intelligent Point class"""


class Point():
    """The point class - contains operators"""
    def __init__(self, coords):
        self.coords = coords
        self.fval = float("inf")

    def eval(self, objfcn):
        """Computes point's function value"""
        self.fval = objfcn(self.coords)
        return self.fval

    def __len__(self):
        return len(self.coords)

    def __str__(self):
        coord = "{:6.3f}, "
        fval = "{:7.4f}"
        format_str = "[" + (coord * len(self)).rstrip(", ") \
                     + "]: " + fval
        return format_str.format(*self.coords, self.fval)

    def __add__(self, other):
        if len(self) != len(other):
            raise IndexError("Dimensions don't match ({}, {})".
                             format(len(self), len(other)))
        return Point([sum(dim) for dim in zip(self, other)])

    def __lt__(self, other):
        return self.fval < other.fval

    def __eq__(self, other):
        return self.fval == other.fval

    def __le__(self, other):
        return (self < other) or (self == other)

    def __ge__(self, other):
        return not (self < other) or (self == other)

    def __gt__(self, other):
        return not self < other

    def __ne__(self, other):
        return not self == other

    def __getitem__(self, key):
        return self.coords[key]

    def __setitem__(self, key, value):
        self.coords[key] = value
