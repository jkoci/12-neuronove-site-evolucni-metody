#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Greedy algorithm optimalizer in 2D """

import matplotlib.pyplot as plt
import numpy as np

from .point import Point
from .vicinity_iterator import VicinityIter
from .vicinity_area import VicinityArea

MODE_APPEND = "a+"


def _update(plot):
    plot.show(block=False)


# pylint: disable=too-many-instance-attributes
class Optimalizer():
    """
    Search for local minimum via hillclimbing algorithm
    """
    max_iter = 100
    iteration = 0

    # Suck on it, pylint, I ain't pushing named tuples on this,
    # so I'm disabling the message here. Defaults are pretty much
    # OK anyway, so most of them will not be overwriten

    # pylint: disable=too-many-arguments
    def __init__(self, objfcn=None, limits=None, vicinity=None,
                 vic_type=None, vic_amount=None, outfile="log.txt"):
        if objfcn is None:
            raise ValueError("You must supply an objfcn")
        self.objfcn = objfcn

        if limits is None:
            raise ValueError("You must supply limits")
        self.limits = limits

        self.vicinity = vicinity or 0.5
        self.vic_type = vic_type or "square"
        self.vic_amount = vic_amount or 100
        self.outfile = outfile

        self.plot = None
    # pylint: enable=too-many-arguments

    def show(self):
        """ Show the graph in blocking context """
        plt.show()
        plt.grid(True)
        return self

    def optimalize(self, first):
        """
        Attempts to find local minimum of self.objfcn
        """
        vic_area = VicinityArea(self.limits)
        self.init_plot()

        # draw contour plot
        # plt.show(block=False)
        with open(self.outfile, MODE_APPEND, encoding="utf-8") as fw:
            curr_best = Point(first)
            curr_best.eval(self.objfcn)
            if curr_best not in vic_area:
                raise ValueError("Initial guess is not inside "
                                 "allowed area")
            fw.write(self.log(curr_best))
            plot_point(curr_best, 'go')

            for iteration in range(1, self.max_iter):
                prev_best = curr_best
                self.iteration = iteration

                vic_it = VicinityIter(vic_size=self.vicinity,
                                      vic_type=self.vic_type,
                                      vic_amnt=self.vic_amount,
                                      lims=self.limits)

                for vic_point in vic_it:
                    candidate = prev_best + vic_point
                    if candidate not in vic_area:
                        continue

                    candidate.eval(self.objfcn)
                    fw.write(self.log(candidate))

                    if candidate < curr_best:
                        curr_best = candidate

                print("Iter {:4d}, best minimum {}".
                      format(iteration, curr_best))
                plot_line(prev_best, curr_best, 'k-')
                plot_point(curr_best, 'ro')

            plot_point(curr_best, 'gx')
            plt.grid(True)
            plt.axis('equal')
            plt.axis((lambda l: [item for sublist in l
                                 for item in sublist])(self.limits))
            self.iteration = 0
            return curr_best

    def log(self, point):
        """Logs candidate point into selected log file"""
        return ("Iter {:4d}, candidate {}\n".
                format(self.iteration, point))

    def evaluate(self, X, Y):
        """
        Evaluates self.objfcn in grid specified by X, Y vectors
        """
        ret = np.zeros((X.shape[0], Y.shape[0]))
        for i, x in enumerate(X):
            for j, y in enumerate(Y):
                ret[i, j] = self.objfcn((x, y))

        return ret

    def init_plot(self):
        """Initializes or returns contour plot of passed objfcn"""
        if self.plot is not None:
            return

        self.plot = True
        plt.figure()

        x = np.linspace(self.limits[0][0], self.limits[0][1],
                        num=500, endpoint=True)
        y = np.linspace(self.limits[1][0], self.limits[1][1],
                        num=500, endpoint=True)
        X, Y = np.meshgrid(x, y)
        Z = self.evaluate(x, y)

        contours = plt.contour(X, Y, Z)
        plt.clabel(contours, inline=1, fontsize=10)

        plt.show(block=False)


def plot_point(point, style):
    """ Plotting a Point """
    plt.plot(point[0], point[1], style)
    plt.show(block=False)


def plot_line(frm, to, style):
    """ Plotting a line between to Points """
    plt.plot((frm[0], to[0]), (frm[1], to[1]), style)
    plt.show(block=False)
