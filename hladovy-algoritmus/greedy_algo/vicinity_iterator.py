#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
VicinityIterator generates points in selected vicinity of zero in
self.dims dimensions. Point interface furthemore allows to __add__
it onto some center Point, resulting in random Point in center's
vicinity.
"""

from numpy.random import normal as _nrand
from numpy.random import uniform as _rand

from .point import Point

KNOWN_TYPES = {
    "square": lambda width, dims: _rand(low=-width,
                                        high=width,
                                        size=dims),

    "circle": lambda width, dims: _nrand(scale=width,
                                         size=dims)
}


class VicinityIter:
    """
    The Iterator
    """
    # pylint: disable=too-many-arguments

    # Suck on it, pylint, I ain't pushing named tuples on this
    # And to complain even more - I could do it in 5 arguments
    # which could be OK. But Python requires to pass self. So
    # I'm disabling the message here.
    def __init__(self, vic_size=None, vic_type=None, vic_amnt=None,
                 dims=None, lims=None):
        self.vic_size = vic_size or 0.1  # length of ruling dim
        self.vic_type = vic_type or KNOWN_TYPES[0]  # type of vicinity
        self.vic_amnt = vic_amnt or 1000  # amount of iterated Points
        self.dims = dims or 2  # generate Point in R**dims

        # supply default limits
        self.lims = lims or ((-10, 10), ) * self.dims
        self.iteration = 0
    # pylint: enable=too-many-arguments

    def __iter__(self):
        if self.vic_type not in KNOWN_TYPES.keys():
            # we can't deal with unknown type
            raise ValueError("Unknown value of vicinity_type ('{}')".
                             format(self.vic_type))

        return self

    def __next__(self):
        if self.iteration < self.vic_amnt:
            # tmp is list of coordinates
            tmp = KNOWN_TYPES[self.vic_type](self.vic_size,
                                             self.dims)

            self.iteration += 1
            # we pack them into a Point here - casting Point directly
            # result in not readable code
            return Point(tmp)

        raise StopIteration
