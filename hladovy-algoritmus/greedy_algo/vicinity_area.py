#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Provides class that can check whether Point is in specified borders
"""


# pylint can suck on my mighty class - I need the operator
# pylint: disable=too-few-public-methods
class VicinityArea:
    """The class - contains 'in' operator"""
    def __init__(self, lims):
        self.lims = lims

    def __contains__(self, point):
        valid = True
        for i in range(1, len(self.lims)):
            lim_min = self.lims[i][0]
            lim_max = self.lims[i][1]
            coord = point[i]
            if not lim_min <= coord <= lim_max:
                valid = False
                break

        return valid
