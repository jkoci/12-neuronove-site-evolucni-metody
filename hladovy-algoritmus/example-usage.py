#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Example usage of Optimalizer() class
"""

import math

from greedy_algo import Optimalizer


def _parabola(X):
    return sum([x**2 for x in X])


def _rastrigin(X):
    return 10 * len(X) + \
        sum([x**2 - 10 * math.cos(2*math.pi*x) for x in X])


def _schwefel(X):
    return 418.9829 * len(X) - \
        sum([x * math.sin(math.sqrt(abs(x)))
             for x in X])


# OBJFCN = _parabola
# LIMITS = ((-10, 10), (-10, 10))

# OBJFCN = _rastrigin
# LIMITS = ((-5, 5), (-5, 5))

OBJFCN = _schwefel
LIMITS = ((-500, 500), (-500, 500))

VICINITY = 10
VIC_TYPE = "square"  # "square" | "circle"
VIC_AMOUNT = 100
MAX_ITER = 500

OPTIM = Optimalizer(objfcn=OBJFCN, limits=LIMITS, vicinity=VICINITY,
                    vic_type=VIC_TYPE, vic_amount=VIC_AMOUNT)
# GUESSES = ((2, 2), (400, 400))
GUESSES = ((5, 3), (400, 400), (150, 150))
for i in GUESSES:
    ret = OPTIM.optimalize(i)
    print("# From starting point {} this local minima was found: {}".
          format(i, ret))

OPTIM.show()
