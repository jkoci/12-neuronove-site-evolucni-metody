#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
GUI interface for greedy_algo's Optimalizer()
"""

import tkinter
from tkinter import messagebox as tkMessageBox
from tkinter import Tk
from tkinter import ttk


def _quit():
    import sys
    sys.exit(0)


def _help():
    tkMessageBox.showinfo("Help", """\
This is brief help for this app.

* Put name of log file into "Log file:" field
* Put vicinity width (extent) into "Vicinity width:" field
* Put vicinity type into "Vicinity type:" field ('square' or 'circle')
* Put vicinity size (how many points are generated in each vicinity) \
into "Vicinity size:" field
* Put maximum interation limit into "Max iterations:" field
* Limits is expected to be two-element python-like tuple
* Guess is expected to be two-element python-like tuple
* Return value is the result of optimization
* If objective function is not selected, parabola is default

If you want to change some settings in more precise manner, forget
the Shitty GUI app and look into examaple-usage.py file.
""")


def _create_tuple(string):
    return tuple(float(x) for x in string[1:-1].split(','))


class App:
    # pylint: disable=too-few-public-methods
    # pylint: disable=too-many-instance-attributes

    """
    The GUI definition
    Warning: ugly
    """
    def __init__(self):
        # pylint: disable=too-many-locals
        # pylint: disable=too-many-statements
        # Root window
        top = Tk()
        top.title("Ugly GUI")

        self.logfile = tkinter.StringVar(top)
        self.logfile.set("log.txt")

        self.vic_size = tkinter.IntVar(top)
        self.vic_amnt = tkinter.IntVar(top)
        self.vic_type = tkinter.StringVar(top)
        self.limits = tkinter.StringVar(top)

        self.vic_size.set(1)
        self.vic_amnt.set(10)
        self.vic_type.set("circle")

        self.max_iter = tkinter.IntVar(top)
        self.max_iter.set(100)

        self.guess = tkinter.StringVar(top)
        self.guess.set("(0,0)")

        self.limits = tkinter.StringVar(top)
        self.limits.set("(-10, 10)")

        self.ret = tkinter.StringVar(top)
        self.ret.set("[?, ?]: ?")

        self.objfcn = tkinter.StringVar(top)

        obj_fcn_grp = ttk.LabelFrame(top,
                                     text="Select Objective function")
        buttons = ttk.Frame(top)
        opts = ttk.LabelFrame(top, text="Options")

        btn_exit = ttk.Button(top, text="Close",
                              command=_quit)
        btn_run = ttk.Button(top, text="Run",
                             command=self._run)
        btn_help = ttk.Button(top, text="Help",
                              command=_help)

        lb_logfile = ttk.Label(top, text="Log file:")
        in_logfile = ttk.Entry(top, textvariable=self.logfile)

        lb_vic_size = ttk.Label(top, text="Vicinity width:")
        in_vic_size = ttk.Entry(top, textvariable=self.vic_size)

        lb_vic_type = ttk.Label(top, text="Vicinity type:")
        in_vic_type = ttk.Entry(top, textvariable=self.vic_type)

        lb_vic_amnt = ttk.Label(top, text="Vicinity size:")
        in_vic_amnt = ttk.Entry(top, textvariable=self.vic_amnt)

        lb_max_iter = ttk.Label(top, text="Max iterations:")
        in_max_iter = ttk.Entry(top, textvariable=self.max_iter)

        lb_limits = ttk.Label(top, text="Limits:")
        in_limits = ttk.Entry(top, textvariable=self.limits)

        lb_guess = ttk.Label(top, text="Guess:")
        in_guess = ttk.Entry(top, textvariable=self.guess)

        lb_ret = ttk.Label(top, text="Return value:")
        in_ret = ttk.Entry(top, textvariable=self.ret)

        lb_logfile.pack()
        in_logfile.pack()
        lb_vic_size.pack()
        in_vic_size.pack()
        lb_vic_type.pack()
        in_vic_type.pack()
        lb_vic_amnt.pack()
        in_vic_amnt.pack()
        lb_max_iter.pack()
        in_max_iter.pack()
        lb_limits.pack()
        in_limits.pack()
        lb_guess.pack()
        in_guess.pack()
        lb_ret.pack()
        in_ret.pack()

        obj_fcns = ("Rastrigin", "Parabola", "Schwefel")
        obj_fcns_radios = (ttk.Radiobutton(obj_fcn_grp, text=fun,
                                           value=fun,
                                           variable=self.objfcn)
                           for fun in obj_fcns)

        for i, radio_btn in enumerate(obj_fcns_radios):
            radio_btn.grid(column=1, row=i, sticky="w")

        btn_exit.pack()
        btn_run.pack()
        btn_help.pack()

        buttons.pack()
        opts.pack()
        obj_fcn_grp.pack()

        top.mainloop()

    def _run(self):
        import math
        from greedy_algo import Optimalizer

        def _parabola(X):
            return sum([x**2 for x in X])

        def _rastrigin(X):
            return 10 * len(X) + \
                sum([x**2 - 10 * math.cos(2*math.pi*x) for x in X])

        def _schwefel(X):
            return 418.9829 * len(X) - \
                sum([x * math.sin(math.sqrt(abs(x)))
                     for x in X])

        fcn_name = self.objfcn.get()
        if fcn_name == "Rastrigin":
            fcn = _rastrigin
        elif fcn_name == "Schwefel":
            fcn = _schwefel
        else:
            fcn = _parabola

        lims = (_create_tuple(self.limits.get()),)*2
        guess = _create_tuple(self.guess.get())

        opt = Optimalizer(objfcn=fcn,
                          limits=lims,
                          vicinity=self.vic_size.get(),
                          vic_type=self.vic_type.get(),
                          vic_amount=self.vic_amnt.get())
        ret = opt.optimalize(guess)

        self.ret.set(ret)

        # pylint: enable=too-many-instance-attributes
        # pylint: enable=too-many-locals
        # pylint: enable=too-many-statements
        # pylint: enable=too-few-public-methods


if __name__ == "__main__":
    App()
